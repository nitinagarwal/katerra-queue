const AWS = require('aws-sdk');
const { getCallerName } = require('./utility');
const context = require('../../context/context');

AWS.config.update({ aws_access_key_id: process.env.AWS_ACCESS_KEY_ID, aws_secret_access_key: process.env.AWS_SECRET_ACCESS_KEY, region: process.env.AWS_REGION });
const sqs = new AWS.SQS({ apiVersion: '2012-11-05' });

async function trigger_queue(messgaeBody, queue_url) {
  try {
    const date = new Date();
    let principal = context.get();
    const user_details = principal.thetokenInfo;
    const MessageBody = {
      tenant: user_details.tenant,
      src: getCallerName(),
      body: messgaeBody,
      created_by: {
        user_id: user_details.userId,
        user_name: user_details.userName,
        user_email: user_details.email
      },
      created_at: parseInt(date.getTime()/1000),
    }
    const payload = {
      MessageBody: JSON.stringify(MessageBody),
      QueueUrl: queue_url || process.env.SQS_QUEUE_URL
    }
    const response = await sqs.sendMessage(payload).promise();
    return response;
  } catch (error) {
    console.error(error);
    throw error;
  }
}

async function queue_listener(queue_url, number_of_messages) {
  try {
    const requestParams = {
      QueueUrl: queue_url,
      MaxNumberOfMessages: number_of_messages || 10,
    }
    sqs.receiveMessage(requestParams, (err, data) => {
      if (err) throw err;
      return data;
    })
  } catch (err) {
    console.error(err);
    throw err;
  }
}

exports.trigger_queue = trigger_queue;
exports.queue_listener = queue_listener;